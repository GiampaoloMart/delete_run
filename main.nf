#!/usr/bin/env nextflow

/*
Delete published data. This nextflow script will generate a file delete.sh to run with qsub

Usage:

RUNID=<RUNID>
/hpcnfs/techunits/genomics/work/nextflow run nf/delete-published-demultiplexing.nf  --publishDir /hpcnfs/techunits/genomics/PublicData  --RUNID $RUNID

# verify the file before to launch the next command.
qsub delete.sh

*/

params.RUNID = null
params.dryrun = true
params.limsURL="https://samples.bioinfo-local.ieo.it"
params.publishDir = "/hpcnfs/techunits/genomics/PublicData/"
params.workDir = "/hpcscratch/ieo/bioinfogenomics/"

process PREPARE {
    
    tag "PREPARE RUN DELETION $params.RUNID"
    
	publishDir "outputs/"

    output:
    path 'delete*.sh', emit: deleteScript
    path '*.txt', emit: report
    
    
    """
    output_file=delete.${params.RUNID}.sh
    # Fastq dir for genomic unit
    echo "#copy genomic unit data" > \$output_file
    echo "chmod +w ${params.publishDir}/FASTQ" >> \$output_file
    echo "chmod +w -R  ${params.publishDir}/FASTQ/${params.RUNID}" >> \$output_file
    echo "rm -rf ${params.publishDir}/FASTQ/${params.RUNID}">> \$output_file
    echo "chmod -w ${params.publishDir}/FASTQ">> \$output_file
    
    
    wget  --no-check-certificate  ${params.limsURL}/csv/flowcellinfo?runFolder=${params.RUNID} -O samplesinfo.csv
    
    echo "# Folders to delete:" > report.txt
    echo "" >> report.txt
    echo "${params.publishDir}/FASTQ/${params.RUNID}" >> report.txt

    # get samples from info file
    cat samplesinfo.csv | sed "s/,/ /g" | sed "s/\\.xlsx//g"| cut -f 3,4,5 -d ' '|sort -u | while read user pi date; do
        echo "# Delete in \$user">> \$output_file
        
        piPublishDir="${params.publishDir}/\$pi"
        
        echo "chmod +w \$piPublishDir/\$user/FASTQ">> \$output_file
        echo "chmod +w -R \$piPublishDir/\$user/FASTQ/${params.RUNID}">> \$output_file
        echo "rm -rf \$piPublishDir/\$user/FASTQ/${params.RUNID}">> \$output_file
        echo "chmod -w \$piPublishDir/\$user/FASTQ">> \$output_file
        echo "\$piPublishDir/\$user/FASTQ/${params.RUNID}" >> report.txt
        
    done
     
      
	echo "# Delete undetermined" >> \$output_file
	head -n 1 samplesinfo.csv | sed "s/,/ /g" | sed "s/\\.xlsx//g"| cut -f 3,4,5 -d ' ' |sort -u | while read user pi date; do
        undeterminedPublishDir="${params.publishDir}/Undetermined/FASTQ"
        
        echo "chmod +w \$undeterminedPublishDir">> \$output_file
        
        echo "chmod +w -R \$undeterminedPublishDir/${params.RUNID}">> \$output_file
        
        echo "rm -rf \$undeterminedPublishDir/${params.RUNID}">> \$output_file
        echo "chmod -w  \$undeterminedPublishDir">> \$output_file
        echo "\$undeterminedPublishDir" >> report.txt
     
    done

    echo "# Remove from workdir:"
    echo "rm -rf ${params.workDir}/${params.RUNID}">> \$output_file
    echo "${params.workDir}/${params.RUNID}">> report.txt

    echo "" >> report.txt
	
    """
}


process DELETE {

	tag "RUN DELETION $params.RUNID: dry run=${params.dryrun}"
    
	publishDir "outputs/"

	input:
		path scriptDelete
		path report

	
    script:
	if (params.dryrun)
	"""
	# Don't do anything

    echo "If everything is fine, run qsub delete.${params.RUNID}.sh or restart with the dryrun option disabled" >> ${report}
	"""
	else
	"""
    echo "Delete folders" >> ${report}
	bash ${scriptDelete}
	"""

}

process UPDATE_STATUS {

    secret 'SMTOKEN'

	tag "UPDATE STATUS $params.RUNID: dry run=${params.dryrun}"
    
	publishDir "outputs/"

    output:
        path '*.txt'

	script:
	if (params.dryrun)
	"""
	# Don't do anything
    echo "Would run the following command: \n \
    curl -s   -H \"Authorization: Bearer \\\$SMTOKEN\" -X GET  \"${params.limsURL}/public/batch/Run%20Folder/${params.RUNID}/\" | jq -r '.[] | .processes[].processId' | while read id; do \n\
        echo curl -s   -H \"Authorization: Bearer \\\$SMTOKEN\" -X PUT "${params.limsURL}/public/process/\\\$id/Process%20Status/running/\" \n\
    done" >>  update-report.txt

	"""
	else
	"""
    echo "Update Sample Manager" >>  update-report.txt
    curl  --insecure  -s   -H "Authorization: Bearer \$SMTOKEN" -X GET  "${params.limsURL}/public/batch/Run%20Folder/${params.RUNID}/" | jq -r '.[] | .processes[].processId' | while read id; do
        echo \$id; curl --insecure -s   -H "Authorization: Bearer \$SMTOKEN" -X PUT "${params.limsURL}/public/process/\$id/Process%20Status/running/" >> update-report.txt
    done

	"""


}


workflow {
    PREPARE  | DELETE | UPDATE_STATUS
}



